#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#define MAX_LINE 80 /* The maximum length command */

int cmdCount = 0;
int is_background(char *args[], int *size){
	int background = 0;
	int i = 0;
	do {
		if (strcmp(args[i], "&") == 0){
			if (args[i+1] == NULL){
				background = 1;
				args[i] = NULL;	
			}
		}
		i++;
	}while ( i < *size);	
	if( background == 1){
		*size=*size-1;
	}

	return background;
}

int * chk_if_output(char *args[], int *size){
	int *out = malloc(2 * sizeof(int));
	out[0] = 0; out[1] = 0;
	int i = 0;
	while ( i < *size - 1){
		if (strcmp(args[i],">") == 0){
			out[0] = 1;
			out[1] = i;
			break;
		}
		i++;
	}
		
	return out;
}

int chck_pipe(char * str){
	int pipe = 0;
	int i = 0;
	while ( i < MAX_LINE && str[i] != '\0'){
		if (str[i] == '|'){
			pipe = 1;		
			
		}
		i++;
	}
	return pipe;
}	


void process(char *command, char *args[], int size, int pipe,char *args2[]){
	pid_t pid;
	pid_t pid2;
	int background = is_background(args, &size);
	int pipefd[2];
	int fd = 0;
	int fd2 = 0;
	pid = fork();

	if (pid < 0) {
		fprintf(stderr, "Fork Failed\n");
	}else if (pid == 0) {
		close(STDOUT_FILENO);
		dup(pipefd[1]);
		close(pipefd[0]);
		close(pipefd[1]);
		int *out = chk_if_output(args, &size);
		if(out[0] == 1){	
			int i = out[1];
			while(i < size){
				args[i]= args[i+1];
				i++;
			}	
			fd = open(args[out[1]-1],O_RDONLY,0);
			dup2(fd,STDIN_FILENO);
			close(fd);
			fd2 = creat(args[out[1]],0644);
			dup2(fd2,STDOUT_FILENO);
			close(fd2);
			out[0] = 0;
			out[1] = 0;
			free(out);

		}
		execvp(command, args);
	}else {	
		if(background == 1){
			waitpid(pid, NULL, 0);
			background = 0;
		}
		else if (pipe) {
			pid2 = fork();
			if(pid < 0) {
				fprintf(stderr, "Fork Failed\n");	
			} else if(pid2 == 0) {

				if (pipe){
					close(STDIN_FILENO);
					dup(pipefd[0]);
					close(pipefd[1]);
					close(pipefd[0]);
				}
				int *out = chk_if_output(args, &size);
				if(out[0] == 1){
					int i = out[1];
					while(i < size){
						args[i]= args[i+1];
						i++;
					}
					fd = open(args[out[1]-1],O_RDONLY,0);
					dup2(fd,STDIN_FILENO);
					close(fd);
					fd2 = creat(args[out[1]],0644);
					dup2(fd2,STDOUT_FILENO);
					close(fd2);
					out[0] = 0;
					out[1] = 0;
					free(out);
				}
				execvp(args2[0], args2);
			
			}else{
				pipe = 0;
				close(pipefd[1]);
				close(pipefd[0]);
				waitpid(pid, NULL, 0);
				waitpid(pid2, NULL, 0);
			}
		}else{
			waitpid(pid, NULL, 0);
		}
	}
}

void history(char cmds[][MAX_LINE], int cCnt){	
	if (cmds != NULL){
		printf("%s\n", cmds[1]);
	}
	int i = 0;
	int j = (cCnt-1 % 10);
	while(cmds[j][0] != '\0' && i < 10) {
		printf("%d%s%s\n",j+1," ", cmds[j]);
		if (j != 0){
			j--;
		}else{
			j = 9;
		}
		i++;
	}
}

int main(void) {
	char *args[MAX_LINE/2 + 1]; /* command line arguments */ 
	char *args2[MAX_LINE/2 + 1];	
	int should_run = 1; /* flag to determine when to exit program */
	char cmds[10][MAX_LINE];  
	while (should_run) {
		char *line;
		char *endline; 
		
		printf("Leyden_osh>");
		
		fgets(line, MAX_LINE, stdin);
		
		if((endline = strchr(line, '\n')) != NULL){	
			*endline = '\0';
		}

		cmdCount++;	
		
		strcpy(cmds[(cmdCount-1 % 10)], line);

	char pipe = chck_pipe(line);
			
		if (strcmp((const char *)line, "!!") == 0){
			line = strdup(cmds[((cmdCount-2) % 10)]);
 		}else if (line[0] ==  '!'){
			line[0] = '\0';	
			int i = 0;
			args[i] = strtok(line, " ");
			do{
				args[++i] = strtok(NULL, " ");																}while(args[i] != NULL);
			line = cmds[atoi(args[0])-1%10];

		}
		if (strcmp((const char *)line,"exit") == 0){
			should_run = 0;
		}else if (pipe){
			int i = 0;
			args[i] = strtok(line, " ");
			do{
				if(*args[i] == '|'){
					int j = 0;
					args2[j] = strtok(NULL, " ");
					do{
						args2[++j] = strtok(NULL, " ");
					}while(args2[j] != NULL);
					args[i] = NULL;
				}

				args[++i] = strtok(NULL, " ");
			}while(args[i] != NULL);
			process(args[0],args, i, pipe, args2);
		}else if (strcmp((const char *)line, "history") == 0) {
			history(cmds, cmdCount);
			
		}else{
			int i = 0;
			args[i] = strtok(line, " ");
			do{
				args[++i] = strtok(NULL, " ");
			}while(args[i] != NULL);
	
			process(args[0],args, i, pipe, args2);
		}	
		fflush(stdout);
	} 
	kill(0, SIGINT); 
	kill(0, SIGKILL);
	return 0;
}


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/shm.h> 
#include <sys/stat.h>

#define MAX_FILENAME 250
#define SIZE 10000000

struct filesharing_struct {
	char flag;
	int size;
	char arr[SIZE];
};

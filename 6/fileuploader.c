#include "fileShare.h"

int main() { 
	
	const char *name = "ileyden_filesharing";
	struct filesharing_struct *ptr;
	int shm_fd; 
	char filename[MAX_FILENAME];
	FILE *file;
	int length;
	char *endline;

	printf("%s\n", "Enter the name of the file:");
			
	fgets(filename, MAX_FILENAME, stdin);
	

	if((endline = strchr(filename, '\n')) != NULL){	
		*endline = '\0';
	}

	
	file  = fopen(filename,"r");
	if (file == NULL){
		fprintf(stderr, "File failed open."); 
		return 1;
	}

	fseek(file, 0 , SEEK_END);
	length = ftell(file);
	rewind(file);
	
	shm_fd = shm_open(name, O_CREAT | O_RDWR, 0666);
	
	ftruncate(shm_fd, SIZE);
	
	ptr = (struct filesharing_struct *) mmap(0, SIZE, PROT_WRITE, MAP_SHARED, shm_fd, 0);
	
	ptr->flag = 1;
	ptr->size = sizeof(char) * length;

	fread(ptr->arr,ptr->size,1,file);
	return 0;
}


#include "fileShare.h"

int main() {
	const char *name = "ileyden_filesharing";
	int shm_fd;
	FILE * file;
	const char out_file[] = "output.txt";
	struct filesharing_struct *ptr;
	shm_fd = shm_open(name, O_RDONLY, 0666);
	ptr = (struct filesharing_struct *) mmap(0, SIZE, PROT_READ, MAP_SHARED, shm_fd, 0);
	
	if(ptr->flag == 1) {
		file = fopen(out_file,"w");
		if(file == NULL){
			fprintf(stderr, "Error while opening output file.");
			return 1;
		}
		fwrite(ptr->arr, ptr->size, 1, file);
		if(ferror(file)){
			fprintf(stderr, "Error writing to file.");
			return 2;
		}printf("%s\n","Successful file download.");

	}else {
		printf("%s\n","There is no file to download.");
	}
	shm_unlink(name);
	
	return 0;
}
